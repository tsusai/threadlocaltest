package com.kangjusang.threadlocaltest;

import android.util.Log;

public class ThreadLocalTestClass {

    static ThreadLocal<String> mThreadLocal;

    public ThreadLocalTestClass(String threadName, String className) {

        if (mThreadLocal == null) {
            mThreadLocal = new ThreadLocal<>();
            Log.d ("ThreadLocalTestClass", "[Running in \'" + threadName + "\'] mThreadLocal is NULL. So try to create new instance.");
        }

        if (mThreadLocal.get() == null) {
            mThreadLocal.set(className);
            Log.d ("ThreadLocalTestClass", "[Running in \'" + threadName + "\'] mThreadLocal has no value. So set value : \'" + className + "\'");
        } else {
            String storedValue = mThreadLocal.get();
            Log.d ("ThreadLocalTestClass", "[Running in \'" + threadName + "\'] mThreadLocal has value : \'" + storedValue + "\'");
        }
    }
}
