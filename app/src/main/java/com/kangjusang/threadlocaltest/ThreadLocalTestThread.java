package com.kangjusang.threadlocaltest;

import androidx.annotation.NonNull;

public class ThreadLocalTestThread extends Thread {
    public ThreadLocalTestThread(String name) {
        super(name);
    }

    @Override
    public void run() {
        String threadName = getName();

        ThreadLocalTestClass threadLocalTestClass = new ThreadLocalTestClass(threadName, "ThreadLocalTestClass 1");
        ThreadLocalTestClass threadLocalTestClass2 = new ThreadLocalTestClass(threadName, "ThreadLocalTestClass 2");
    }
}
